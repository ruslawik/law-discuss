<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDiscussesTable extends Migration
{
    public function up()
    {
        Schema::table('discusses', function (Blueprint $table) {
            $table->unsignedInteger('institute_id');
            $table->foreign('institute_id', 'institute_fk_1991548')->references('id')->on('institutes');
        });
    }
}
