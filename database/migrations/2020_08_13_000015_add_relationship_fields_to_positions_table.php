<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPositionsTable extends Migration
{
    public function up()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->unsignedInteger('discuss_rule_id');
            $table->foreign('discuss_rule_id', 'discuss_rule_fk_1991628')->references('id')->on('discusses');
            $table->unsignedInteger('author_id');
            $table->foreign('author_id', 'author_fk_1992844')->references('id')->on('users');
        });
    }
}
