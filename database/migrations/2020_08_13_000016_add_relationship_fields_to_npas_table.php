<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToNpasTable extends Migration
{
    public function up()
    {
        Schema::table('npas', function (Blueprint $table) {
            $table->unsignedInteger('organization_id');
            $table->foreign('organization_id', 'organization_fk_1991328')->references('id')->on('organizations');
        });
    }
}
