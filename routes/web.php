<?php

Route::any('/', 'Front\FrontController@index');
Route::any('/npa/{npa_id}', 'Front\FrontController@npaPage');
Route::any('/institute/{institute_id}', 'Front\FrontController@institutePage');
Route::any('/institute/{institute_id}/discussion/{discuss_id}', 'Front\FrontController@discussPage');
Route::any('/doctrine/{position_id}', 'Front\FrontController@loadDoctrine');
Route::any('/contra_pos/{position_id}', 'Front\FrontController@contraPos');
Route::any('/position_text/{position_id}', 'Front\FrontController@posText');
Route::any('/team', 'Front\FrontController@Team');
Route::any('/base_docs', 'Front\FrontController@baseDocs');
Route::any('/faqs', 'Front\FrontController@Faqs');

Route::any('/loginform', 'Front\FrontController@loginForm')->name('loginform');
Route::any('/registration_form', 'Front\FrontController@registrationForm');
Route::any('/wall/{u_id}', 'Front\FrontController@publicProfile');

Route::any('/select2/institutes/{npa_id}', 'Front\FrontController@select2Institutes');
Route::any('/select2/npas', 'Front\FrontController@select2Npas');
Route::any('/inses_to_search/{npa_id}', 'Front\FrontController@insesToSearch');

Route::post('/front_register_post', 'Front\FrontController@frontReg');
Route::post('/reg/media', 'Front\FrontController@storeMedia')->name('front.reg.storeMedia');
Route::post('/front_login', 'Front\FrontController@frontLogin');



Route::group(['middleware' => ['authruslan']],
    function(){
    Route::any('/cabinet', 'Front\FrontController@cabinet');
    Route::any('/cabinet/positions', 'Front\FrontController@cabinetPositions');
    Route::resource('user', 'Front\FrontController');
    Route::post('/add_position', 'Front\FrontController@addPosition');
});

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // Organizations
    Route::delete('organizations/destroy', 'OrganizationsController@massDestroy')->name('organizations.massDestroy');
    Route::resource('organizations', 'OrganizationsController');

    // Npas
    Route::delete('npas/destroy', 'NpaController@massDestroy')->name('npas.massDestroy');
    Route::post('npas/ckmedia', 'NpaController@storeCKEditorImages')->name('npas.storeCKEditorImages');
    Route::resource('npas', 'NpaController');

    // Institutes
    Route::delete('institutes/destroy', 'InstitutesController@massDestroy')->name('institutes.massDestroy');
    Route::post('institutes/media', 'InstitutesController@storeMedia')->name('institutes.storeMedia');
    Route::post('institutes/ckmedia', 'InstitutesController@storeCKEditorImages')->name('institutes.storeCKEditorImages');
    Route::resource('institutes', 'InstitutesController');

    // Discusses
    Route::delete('discusses/destroy', 'DiscussController@massDestroy')->name('discusses.massDestroy');
    Route::post('discusses/media', 'DiscussController@storeMedia')->name('discusses.storeMedia');
    Route::post('discusses/ckmedia', 'DiscussController@storeCKEditorImages')->name('discusses.storeCKEditorImages');
    Route::resource('discusses', 'DiscussController');

    // Law Systems
    Route::delete('law-systems/destroy', 'LawSystemController@massDestroy')->name('law-systems.massDestroy');
    Route::post('law-systems/media', 'LawSystemController@storeMedia')->name('law-systems.storeMedia');
    Route::post('law-systems/ckmedia', 'LawSystemController@storeCKEditorImages')->name('law-systems.storeCKEditorImages');
    Route::resource('law-systems', 'LawSystemController');

    // Positions
    Route::delete('positions/destroy', 'PositionsController@massDestroy')->name('positions.massDestroy');
    Route::post('positions/media', 'PositionsController@storeMedia')->name('positions.storeMedia');
    Route::post('positions/ckmedia', 'PositionsController@storeCKEditorImages')->name('positions.storeCKEditorImages');
    Route::resource('positions', 'PositionsController');

    // Arguments
    Route::delete('arguments/destroy', 'ArgumentsController@massDestroy')->name('arguments.massDestroy');
    Route::post('arguments/media', 'ArgumentsController@storeMedia')->name('arguments.storeMedia');
    Route::post('arguments/ckmedia', 'ArgumentsController@storeCKEditorImages')->name('arguments.storeCKEditorImages');
    Route::resource('arguments', 'ArgumentsController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
