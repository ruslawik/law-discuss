<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');

    // Organizations
    Route::apiResource('organizations', 'OrganizationsApiController');

    // Npas
    Route::apiResource('npas', 'NpaApiController');

    // Institutes
    Route::post('institutes/media', 'InstitutesApiController@storeMedia')->name('institutes.storeMedia');
    Route::apiResource('institutes', 'InstitutesApiController');

    // Discusses
    Route::post('discusses/media', 'DiscussApiController@storeMedia')->name('discusses.storeMedia');
    Route::apiResource('discusses', 'DiscussApiController');

    // Law Systems
    Route::post('law-systems/media', 'LawSystemApiController@storeMedia')->name('law-systems.storeMedia');
    Route::apiResource('law-systems', 'LawSystemApiController');

    // Positions
    Route::post('positions/media', 'PositionsApiController@storeMedia')->name('positions.storeMedia');
    Route::apiResource('positions', 'PositionsApiController');

    // Arguments
    Route::post('arguments/media', 'ArgumentsApiController@storeMedia')->name('arguments.storeMedia');
    Route::apiResource('arguments', 'ArgumentsApiController');
});
