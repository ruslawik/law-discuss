@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.discuss.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.discusses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.id') }}
                        </th>
                        <td>
                            {{ $discuss->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.name') }}
                        </th>
                        <td>
                            {{ $discuss->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.rule') }}
                        </th>
                        <td>
                            {!! $discuss->rule !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.institute') }}
                        </th>
                        <td>
                            {{ $discuss->institute->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.discusses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#discuss_rule_positions" role="tab" data-toggle="tab">
                {{ trans('cruds.position.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="discuss_rule_positions">
            @includeIf('admin.discusses.relationships.discussRulePositions', ['positions' => $discuss->discussRulePositions])
        </div>
    </div>
</div>

@endsection