<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="irstheme">

    <title> Обсуждение законодательства </title>
    
    <link href="/front/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/front/assets/css/flaticon.css" rel="stylesheet">
    <link href="/front/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/assets/css/animate.css" rel="stylesheet">
    <link href="/front/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="/front/assets/css/owl.theme.css" rel="stylesheet">
    <link href="/front/assets/css/slick.css" rel="stylesheet">
    <link href="/front/assets/css/slick-theme.css" rel="stylesheet">
    <link href="/front/assets/css/swiper.min.css" rel="stylesheet">
    <link href="/front/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="/front/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/front/assets/css/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">

    <!-- start preloader -->
    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>        
    </div>
    <!-- end preloader -->


        @include("front._partials.navbar")


        @yield("content")


        <!-- start site-footer -->
        <footer class="site-footer">
            <div class="social-newsletter-area">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="social-newsletter-content clearfix">
                                <div class="social-area">
                                    <ul class="clearfix">
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                                    </ul>
                                </div>
                                <div class="logo-area">
                                    <img src="/front/assets/images/logokazguu.png" width="120" alt>
                                </div>
                                <div class="newsletter-area">
                                    <div class="inner">
                                        <h3>Рассылка</h3>
                                        <form>
                                            <div class="input-1">
                                                <input type="email" class="form-control" placeholder="Эл. почта *" required="">
                                            </div>
                                            <div class="submit clearfix">
                                                <button type="submit"><i class="fi flaticon-paper-plane"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="separator"></div>
                        <div class="col col-xs-12">
                            <p class="copyright">Copyright &copy; 2020 KAZGUU University. All rights reserved.</p>
                            <div class="extra-link">
                                <ul>
                                    <li><a href="#">Политика конфиденциальности</a></li>
                                    <li><a href="#">О нас</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->
    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="/front/assets/js/jquery.min.js"></script>
    <script src="/front/assets/js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="/front/assets/js/jquery-plugin-collection.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>

    <!-- Custom script for this template -->
    <script src="/front/assets/js/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    @yield("js")
</body>
</html>
    @yield("modals")
