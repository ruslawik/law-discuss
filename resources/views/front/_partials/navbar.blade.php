<!-- Start header -->
        <header id="header" class="site-header header-style-1">

            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="/front/assets/images/logokazguu.png" width="100" style="margin-top: -10px !important;" alt></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="ti-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/">Главная</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Обсуждаемые НПА</a>
                                <ul class="sub-menu">
                                    @foreach ($navbar['npas'] as $npa)
                                        <li><a href="/npa/{{ $npa['id'] }}">{{ $npa['name'] }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a href="/team">Команда</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/base_docs">Основные документы</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="/faqs">ЧАВо</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                @if(Auth::check())
                                    <a class="theme-btn" href="/cabinet">Личный кабинет</a>
                                @else
                                    <a class="theme-btn" href="/loginform">Вход/Регистрация</a>
                                @endif
                            </li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->