@extends('front.layouts.page_layout')
@section('content')

	   <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ $npa_data[0]['name'] }}</h2>
                        <p>Обсуждение законодательства</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

        <!-- start service-single-section -->
        <section class="service-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-9 col-md-push-3">
                        <div class="service-single-content">

                            <!--<h2>{{ $npa_data[0]['name'] }}</h2>!-->
            <div class="container">   
                <div class="row">
                    <div class="col col-xs-9">

                    	<h4>Выберите институт в меню слева для начала работы с данным НПА.</h4>
                    	<hr>

                    	{!! $npa_data[0]['description'] !!}

                    </div>
                </div>    
            </div> <!-- end container -->
        <!-- end faq-pg-section --> 



                          
                        </div>
                    </div>
                    <div class="col col-md-3 col-md-pull-9">
                        <div class="service-sidebar">
                            <div class="widget service-list-widget">
                                
                                	@if($has_institutes == 1)
                                		<ul>
                                			@foreach ($npa_data[0]->npaInstitutes as $institute)
                                				<li><a href="/institute/{{$institute['id']}}">{{ $institute['name'] }}</a></li>
                                			@endforeach
                                		</ul>
                                	@else
                                		<h4>В данный НПА не добавлен ни один институт права для обсуждения. </h4>
                            			<hr>
                                	@endif
                            </div>
                            <div class="widget contact-widget">
                                <div>
                                    <h4>Нужна помощь?</h4>
                                    <p>Вы можете обратиться в Telegram-бот по адресу gkrk.kz для получения помощи в вопросах участия в обсуждении или иных, интересующих Вас темах.</p>
                                    <a href="#">Перейти</a>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end service-single-section -->


@endsection