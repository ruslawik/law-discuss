<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            <img src="/front/assets/images/logokazguu.png" width=60>
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item" style="display: none;">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('organization_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.organizations.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/organizations') || request()->is('admin/organizations/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-sitemap c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.organization.title') }}
                </a>
            </li>
        @endcan
        @can('npa_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.npas.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/npas') || request()->is('admin/npas/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-university c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.npa.title') }}
                </a>
            </li>
        @endcan
        @can('law_system_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.law-systems.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/law-systems') || request()->is('admin/law-systems/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-gavel c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.lawSystem.title') }}
                </a>
            </li>
        @endcan
        @can('institute_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.institutes.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/institutes') || request()->is('admin/institutes/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-list-ol c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.institute.title') }}
                </a>
            </li>
        @endcan
        @can('discuss_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.discusses.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/discusses') || request()->is('admin/discusses/*') ? 'active' : '' }}">
                    <i class="fa-fw fab fa-discord c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.discuss.title') }}
                </a>
            </li>
        @endcan
        @can('position_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.positions.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/positions') || request()->is('admin/positions/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-crosshairs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.position.title') }}
                </a>
            </li>
        @endcan
        
        @can('argument_access')
            <li class="c-sidebar-nav-item" style="display: none;">
                <a href="{{ route("admin.arguments.index") }}" class="c-sidebar-nav-link {{ request()->is('admin/arguments') || request()->is('admin/arguments/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-balance-scale c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.argument.title') }}
                </a>
            </li>
        @endcan
        
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>