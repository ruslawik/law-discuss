<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDiscussRequest;
use App\Http\Requests\StoreDiscussRequest;
use App\Http\Requests\UpdateDiscussRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\StoreUserFrontRequest;
use App\Http\Requests\UpdateUserSelfRequest;
use App\Models\Discuss;
use App\Models\Institute;
use App\Models\Npa;
use App\Models\Position;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Auth;
use App\Models\Argument;

class FrontController extends Controller
{
	use MediaUploadingTrait;

	public function navbar (){
		$navbar = [];
		$navbar['npas'] = Npa::all();
		return $navbar;
	}

    public function index (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();

        return view("front.index", $ar);
    }

    public function npaPage ($npa_id){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['npa_data'] = Npa::where('id', $npa_id)->get();

    	if(Institute::where('npa_id', $npa_id)->count() == 0){
    		$ar['has_institutes'] = 0;
    	}else{
    		$ar['has_institutes'] = 1;
    	}

    	return view("front.npa_page", $ar);
    }

    public function institutePage ($institute_id){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['institute_data'] = Institute::where('id', $institute_id)->get();
    	$ar['discuss_rules'] = Discuss::where('institute_id', $institute_id)->get();

    	if(Discuss::where('institute_id', $institute_id)->count() == 0){
    		$ar['has_rules'] = 0;
    	}else{
    		$ar['has_rules'] = 1;
    	}

    	return view("front.institute_page", $ar);

    }

    public function discussPage ($institute_id, $discuss_id){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['discuss_rule_data'] = Discuss::where("id", $discuss_id)->get();
    	$ar['institute_data'] = Institute::where('id', $institute_id)->get();
    	$ar['discuss_rules'] = Discuss::where('institute_id', $institute_id)->get();
    	$ar['positions'] = Position::where('discuss_rule_id', $discuss_id)
    									->where('approved', 'published')
    									->paginate(20);

    	return view("front.discuss_page", $ar);

    }

    public function loadDoctrine ($position_id){

    	$doctrine = Position::where('id', $position_id)->get();
    	print($doctrine['0']->doctrine);
    }

    public function contraPos ($position_id){

    	$positions_contra = Position::where('vs_position_id', $position_id)->get();
    	$positions_contra_num = Position::where('vs_position_id', $position_id)->count();

    	if($positions_contra_num == 0){
    		print("<h4>Нет позиций против</h4>");
    	}else{
    		foreach ($positions_contra as $position) {
    			print('<img src="'.$position->author->photo['url'].'" width="50px" style="border-radius:50%;">&nbsp;&nbsp;');
    			print($position->author->name." ".$position->author->surname." ".$position->author->last_name."<br><br>");
    			print($position['position']."<br>");
    			$arg_num = 1;
    			foreach ($position['positionArguments'] as $argument) {
    				print('<img src="/front/assets/images/tick.jpg" width=20> <b>Аргумент '.$arg_num.': </b>');
    				print($argument['argument']."<br>");
    				$arg_num++;
    			}
    			print("<hr>");
    		}
    	}

    }


    public function loginForm (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['action'] = action("Front\FrontController@frontLogin");

    	return view("front.loginform", $ar);
    }

    public function registrationForm (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['action'] = action("Front\FrontController@frontReg");

    	return view("front.registration_form", $ar);
    }

    public function cabinet (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = Auth::user();
    	//$ar['action'] = action("Front\FrontController@updateUserData");

    	return view('front.cabinet', $ar);
    }

    public function frontReg (StoreUserFrontRequest $request){

    	$user = User::create($request->all());

        $user->roles()->sync($request->input('roles', []));

        /*
        if ($request->input('photo', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }*/

        if ($request->input('education_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
        }

        if ($request->input('degree_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
        }

        return redirect()->back()->with('success', 'Ваша заявка успешно отправлена на рассмотрение модератором!');
    }

    public function update (UpdateUserSelfRequest $request, User $user)
    {	
    	foreach ($request->input('roles') as $role) {
    		if($role == 4 && $user->roles->contains(3)){
    			$user->update(["approved" => "0"]);
    		}
    	}
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        
        if ($request->input('photo', false)) {
            if (!$user->photo || $request->input('photo') !== $user->photo->file_name) {
                if ($user->photo) {
                    $user->photo->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($user->photo) {
            $user->photo->delete();
        }

        if ($request->input('education_diploma', false)) {
            if (!$user->education_diploma || $request->input('education_diploma') !== $user->education_diploma->file_name) {
                if ($user->education_diploma) {
                    $user->education_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
            }
        } elseif ($user->education_diploma) {
            $user->education_diploma->delete();
        }

        if ($request->input('degree_diploma', false)) {
            if (!$user->degree_diploma || $request->input('degree_diploma') !== $user->degree_diploma->file_name) {
                if ($user->degree_diploma) {
                    $user->degree_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
            }
        } elseif ($user->degree_diploma) {
            $user->degree_diploma->delete();
        }

        return redirect()->back()->with('success', 'Данные успешно обновлены!');;
    }

    public function str_limit($value, $limit = 40, $end = '...')
	{
    	$limit = $limit - mb_strlen($end); // Take into account $end string into the limit
    	$valuelen = mb_strlen($value);
    	return $limit < $valuelen ? mb_substr($value, 0, mb_strrpos($value, ' ', $limit - $valuelen)) . $end : $value;
	}

    public function addPosition (Request $request){
    	$pro_contra = $request->input("pro_contra");
    	$position_text = $request->input("position_text");
    	$doctrine_text = $request->input("doctrine_text");
    	$title = $this->str_limit(strip_tags($position_text));
    	$discuss_rule_id = $request->input("discuss_rule_id");
    	$author_id = Auth::user()->id;

    	$position = new Position();
    	$position->position = $position_text;
    	$position->doctrine = $doctrine_text;
    	$position->title = $title;
    	$position->approved = "moder";
    	$position->pro_contra = $pro_contra;
    	$position->discuss_rule_id = $discuss_rule_id;
    	$position->author_id = $author_id;
    	if($request->has('position_id_vs'))
    		$position->vs_position_id = $request->input("position_id_vs"); 
    	$position->save();

    	$position_id = $position->id;

    	$arg_num = $request->input("arg_number");
    	for($i=1; $i<=$arg_num; $i++){
    		$argument = new Argument();
    		$argument->argument =  $request->input("arg_".$i);
    		$argument->position_id =  $position_id;
    		$argument->save();
    	}

    	return redirect()->back()->with('success', 'Ваша позиция успешно отправлена на проверку модератором! <br> Вы можете просмотреть статус написанных Вами позиций <a href="/cabinet/positions">в личном кабинете.</a>');

    }

    public function cabinetPositions (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = Auth::user();
    	$ar['positions'] = Position::where("author_id", Auth::user()->id)->orderBy("id", "DESC")->get();

    	return view("front.cabinet_positions", $ar);
    }

    public function publicProfile ($user_id){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = User::where('id', $user_id)->get();
    	$ar['positions'] = Position::where("author_id", $user_id)->orderBy("id", "DESC")->get();

    	return view("front.public_profile", $ar);
    }

    public function posText ($position_id){

    	$text = Position::where('id', $position_id)->get();
    	print($text['0']->position);
    }

    public function select2Institutes ($npa_id){

    	$data = Institute::where('npa_id', $npa_id)->get();
    	foreach ($data as $institute) {
    		$select2[] = array("id"=>$institute->id, "text"=> $institute->name);
    	}
    	echo json_encode($select2);
    }

    public function select2Npas (){

    	$data = Npa::all();
    	foreach ($data as $npa) {
    		$select2[] = array("id"=>$npa->id, "text"=> $npa->name);
    	}
    	echo json_encode($select2);
    }

    public function Team (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.team", $ar);
    }

    public function Faqs (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.faqs", $ar);
    }

    public function baseDocs (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.base_docs", $ar);
    }

    public function insesToSearch ($npa_id){

    	$data = Institute::where('npa_id', $npa_id)->get();
    	print('<option value>Все</option>');
    	foreach ($data as $institute) {
    		print('<option value="'.$institute->name.'">'.$institute->name.'</option>');
    	}
    }

}









