<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyNpaRequest;
use App\Http\Requests\StoreNpaRequest;
use App\Http\Requests\UpdateNpaRequest;
use App\Models\Npa;
use App\Models\Organization;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class NpaController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('npa_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Npa::with(['organization'])->select(sprintf('%s.*', (new Npa)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'npa_show';
                $editGate      = 'npa_edit';
                $deleteGate    = 'npa_delete';
                $crudRoutePart = 'npas';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->addColumn('organization_name', function ($row) {
                return $row->organization ? $row->organization->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'organization']);

            return $table->make(true);
        }

        return view('admin.npas.index');
    }

    public function create()
    {
        abort_if(Gate::denies('npa_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.npas.create', compact('organizations'));
    }

    public function store(StoreNpaRequest $request)
    {
        $npa = Npa::create($request->all());

        return redirect()->route('admin.npas.index');
    }

    public function edit(Npa $npa)
    {
        abort_if(Gate::denies('npa_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $npa->load('organization');

        return view('admin.npas.edit', compact('organizations', 'npa'));
    }

    public function update(UpdateNpaRequest $request, Npa $npa)
    {
        $npa->update($request->all());

        return redirect()->route('admin.npas.index');
    }

    public function show(Npa $npa)
    {
        abort_if(Gate::denies('npa_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npa->load('organization', 'npaInstitutes');

        return view('admin.npas.show', compact('npa'));
    }

    public function destroy(Npa $npa)
    {
        abort_if(Gate::denies('npa_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npa->delete();

        return back();
    }

    public function massDestroy(MassDestroyNpaRequest $request)
    {
        Npa::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        $model         = new Npa();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
