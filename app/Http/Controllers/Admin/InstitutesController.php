<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyInstituteRequest;
use App\Http\Requests\StoreInstituteRequest;
use App\Http\Requests\UpdateInstituteRequest;
use App\Models\Institute;
use App\Models\LawSystem;
use App\Models\Npa;
use Gate;
use App\Models\Discuss;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class InstitutesController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('institute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Institute::with(['npa'])->select(sprintf('%s.*', (new Institute)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'institute_show';
                $editGate      = 'institute_edit';
                $deleteGate    = 'institute_delete';
                $crudRoutePart = 'institutes';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->addColumn('npa_name', function ($row) {
                return $row->npa ? $row->npa->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'npa']);

            return $table->make(true);
        }

        $npas = Npa::get();

        return view('admin.institutes.index', compact('npas'));
    }

    public function create()
    {
        abort_if(Gate::denies('institute_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npas = Npa::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.institutes.create', compact('npas'));
    }

    public function store(StoreInstituteRequest $request)
    {
        $institute = Institute::create($request->all());
        /*
        $lawsystems = LawSystem::all();

        foreach ($lawsystems as $lawsystem) {
            $discuss = new Discuss();
            $discuss->rule = "Текст нормы для обсуждения";
            $discuss->institute_id = $institute->id;
            $discuss->name = $lawsystem->name;
            $discuss->save();
            $discuss->addMedia($lawsystem->avatar['url'])->toMediaCollection('avatar');
        }
        */
        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $institute->id]);
        }

        return redirect()->route('admin.institutes.index');
    }

    public function edit(Institute $institute)
    {
        abort_if(Gate::denies('institute_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npas = Npa::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $institute->load('npa');

        return view('admin.institutes.edit', compact('npas', 'institute'));
    }

    public function update(UpdateInstituteRequest $request, Institute $institute)
    {
        $institute->update($request->all());

        return redirect()->route('admin.institutes.index');
    }

    public function show(Institute $institute)
    {
        abort_if(Gate::denies('institute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institute->load('npa');

        return view('admin.institutes.show', compact('institute'));
    }

    public function destroy(Institute $institute)
    {
        abort_if(Gate::denies('institute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        Discuss::where('institute_id', $institute->id)->delete();
        $institute->delete();

        return back();
    }

    public function massDestroy(MassDestroyInstituteRequest $request)
    {
        Institute::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('institute_create') && Gate::denies('institute_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Institute();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
