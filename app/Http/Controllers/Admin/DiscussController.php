<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDiscussRequest;
use App\Http\Requests\StoreDiscussRequest;
use App\Http\Requests\UpdateDiscussRequest;
use App\Models\Discuss;
use App\Models\Institute;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DiscussController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('discuss_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Discuss::with(['institute'])->select(sprintf('%s.*', (new Discuss)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'discuss_show';
                $editGate      = 'discuss_edit';
                $deleteGate    = 'discuss_delete';
                $crudRoutePart = 'discusses';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->addColumn('institute_name', function ($row) {
                return $row->institute ? $row->institute->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'institute']);

            return $table->make(true);
        }

        $institutes = Institute::get();

        return view('admin.discusses.index', compact('institutes'));
    }

    public function create()
    {
        abort_if(Gate::denies('discuss_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.discusses.create', compact('institutes'));
    }

    public function store(StoreDiscussRequest $request)
    {
        $discuss = Discuss::create($request->all());

        if ($request->input('avatar', false)) {
            $discuss->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $discuss->id]);
        }

        return redirect()->route('admin.discusses.index');
    }

    public function edit(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $discuss->load('institute');

        return view('admin.discusses.edit', compact('institutes', 'discuss'));
    }

    public function update(UpdateDiscussRequest $request, Discuss $discuss)
    {
        $discuss->update($request->all());

        if ($request->input('avatar', false)) {
            if (!$discuss->avatar || $request->input('avatar') !== $discuss->avatar->file_name) {
                if ($discuss->avatar) {
                    $discuss->avatar->delete();
                }

                $discuss->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
            }
        } elseif ($discuss->avatar) {
            $discuss->avatar->delete();
        }

        return redirect()->route('admin.discusses.index');
    }

    public function show(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $discuss->load('institute', 'discussRulePositions');

        return view('admin.discusses.show', compact('discuss'));
    }

    public function destroy(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $discuss->delete();

        return back();
    }

    public function massDestroy(MassDestroyDiscussRequest $request)
    {
        Discuss::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('discuss_create') && Gate::denies('discuss_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Discuss();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
