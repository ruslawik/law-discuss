<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserResource(User::with(['roles', 'organization'])->get());
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        if ($request->input('photo', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($request->input('education_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
        }

        if ($request->input('degree_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
        }

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserResource($user->load(['roles', 'organization']));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        if ($request->input('photo', false)) {
            if (!$user->photo || $request->input('photo') !== $user->photo->file_name) {
                if ($user->photo) {
                    $user->photo->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($user->photo) {
            $user->photo->delete();
        }

        if ($request->input('education_diploma', false)) {
            if (!$user->education_diploma || $request->input('education_diploma') !== $user->education_diploma->file_name) {
                if ($user->education_diploma) {
                    $user->education_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
            }
        } elseif ($user->education_diploma) {
            $user->education_diploma->delete();
        }

        if ($request->input('degree_diploma', false)) {
            if (!$user->degree_diploma || $request->input('degree_diploma') !== $user->degree_diploma->file_name) {
                if ($user->degree_diploma) {
                    $user->degree_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
            }
        } elseif ($user->degree_diploma) {
            $user->degree_diploma->delete();
        }

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
