<?php

namespace App\Http\Middleware;
use Closure;

class AuthRuslan
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guest()) {
            return redirect()->route('loginform');
        }

        return $next($request);
    }
}
