<?php

namespace App\Http\Requests;

use App\Models\Npa;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateNpaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('npa_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'            => [
                'string',
                'required',
            ],
            'organization_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
