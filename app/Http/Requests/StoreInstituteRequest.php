<?php

namespace App\Http\Requests;

use App\Models\Institute;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreInstituteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('institute_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'   => [
                'string',
                'required',
            ],
            'npa_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
