<?php

namespace App\Http\Requests;

use App\Models\Discuss;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreDiscussRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('discuss_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'         => [
                'string',
                'nullable',
            ],
            'rule'         => [
                'required',
            ],
            'institute_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
