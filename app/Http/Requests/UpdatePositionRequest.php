<?php

namespace App\Http\Requests;

use App\Models\Position;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePositionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('position_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'approved'        => [
                'required',
            ],
            'author_id'       => [
                'required',
                'integer',
            ],
            'title'           => [
                'string',
                'required',
            ],
            'position'        => [
                'required',
            ],
            'discuss_rule_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
