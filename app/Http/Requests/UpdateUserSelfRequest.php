<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Auth;

class UpdateUserSelfRequest extends FormRequest
{

    public function rules()
    {
        return [
            'roles.*'   => [
                'integer',
            ],
            'roles'     => [
                'required',
                'array',
            ],
            'name'      => [
                'string',
                'required',
            ],
            'surname'   => [
                'string',
                'required',
            ],
            'last_name' => [
                'string',
                'nullable',
            ],
            'phone'     => [
                'string',
                'nullable',
            ],
            'job_place' => [
                'string',
                'nullable',
            ],
        ];
    }
}
