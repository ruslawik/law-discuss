<?php

namespace App\Http\Requests;

use App\Models\Argument;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreArgumentRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('argument_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'argument'    => [
                'required',
            ],
            'position_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
