<?php

namespace App\Http\Requests;

use App\Models\LawSystem;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyLawSystemRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('law_system_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:law_systems,id',
        ];
    }
}
