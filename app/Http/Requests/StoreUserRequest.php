<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreUserRequest extends FormRequest
{   
    /*/public/storage*/
    public function authorize()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'roles.*'   => [
                'integer',
            ],
            'roles'     => [
                'required',
                'array',
            ],
            'name'      => [
                'string',
                'required',
            ],
            'surname'   => [
                'string',
                'required',
            ],
            'last_name' => [
                'string',
                'nullable',
            ],
            'email'     => [
                'required',
                'unique:users',
            ],
            'phone'     => [
                'string',
                'nullable',
            ],
            'job_place' => [
                'string',
                'nullable',
            ],
            'password'  => [
                'required',
            ],
        ];
    }
}
