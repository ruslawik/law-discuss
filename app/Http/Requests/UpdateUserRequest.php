<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'roles.*'   => [
                'integer',
            ],
            'roles'     => [
                'required',
                'array',
            ],
            'name'      => [
                'string',
                'required',
            ],
            'surname'   => [
                'string',
                'required',
            ],
            'last_name' => [
                'string',
                'nullable',
            ],
            'email'     => [
                'required',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'phone'     => [
                'string',
                'nullable',
            ],
            'job_place' => [
                'string',
                'nullable',
            ],
        ];
    }
}
